# 2D Aerodynamics Machine Learning

This space is dedicated to the organization of multiple projects that focus on the application of machine learning to 2D aerodynamics.


We are currently in the process of publishing a paper about the AirfoilMNIST Dataset and the corresponding algorithms.
We will add this reference and proper citation as soon as possible. 


## AirfoilMNIST Dataset

`Important information:` The mediaTUM link has been updated to show the new version of the dataset. 

The `AirfoilMNIST` dataset comprises of 2D RANS simulations of NACA airfoils. It is divided into multiple subsets, which are available on [mediaTUM](https://mediatum.ub.tum.de/1745500):
- `AirfoilMNIST-raw`
This subset refers to the raw CFD output from the `nacaFOAM` pipeline with a total of 148258 samples. This subset shall give researchers the maximum freedom in manipulating and processing the data to their desire but requires the most storage capacity with around 3.5 TB.
- `AirfoilMNIST`
The second subset is an already preprocessed version of the `AirfoilMNIST-raw` subset featuring a fully randomized train-test split [80, 20]. By remapping the simulation grid onto a uniform grid and saving the samples into the TFRecord file format, the effective storage size is reduced to around 500 GB which makes it much more accessible.
- `AirfoilMNIST-incompressible`
The third subset is a further simplification of the `AirfoilMNIST` dataset. It uses the same logic of remapping the data and splitting the data into encoder and decoder inputs. However, rather than converting the entire raw dataset, it only uses samples where the Mach number satisfies the incompressible flow assumption, thus M < 0.3. This gives a total of 78 010 samples. As incompressible flows are typically only classified through the velocity vector and pressure field, all other flow fields are omitted for this subset. Additionally, this dataset has been normalized by the respective freestream condition. The train-test split has been adapted so that for every combination of airfoil geometry and angle of attack, one randomly chosen velocity is transferred to the test dataset. This gives 64 669 train samples and 13 341 test samples, which leads to a train-test split of [82.9, 17.1]. This dataset requires the least storage capacity, around 50GB.
- `BluffBodyExtension-raw`
The extension of 2D bluff body simulations is only available for the `airfoilMNIST-incompressible` subset. This subset refers to the raw CFD output from the BluffFOAM pipeline with 11 760 samples. This dataset extension gives the maximum freedom in manipulating and processing the raw data but requires the most storage capacity, around 125GB
- `BluffBodyExtension` 
This subset is an already preprocessed version of the `BluffBodyExtension-raw`. It gives 9 954 train samples and 1 806 test samples, which leads to a train-test split of [84.6, 15.4]. This dataset extension requires only around 12GB of storage capacity.

A `data nutrition label` for the dataset can be found [here](https://datanutrition.org/labels/v3/?id=03009e8d-8375-4f5f-835b-8ca582a7f6c7). The `croissant meta data` .JSON-LD files are available [here](https://gitlab.lrz.de/2DAeroML/gitlab-profile).


## Data Generation

This subgroup contains the projects `nacaFOAM`, `bluffFOAM` and `Interpolation`, which were used to generate the `AirfoilMNIST` dataset and its `bluff body extension`.

### nacaFOAM

`nacaFOAM` is a CFD simulation pipeline using the `OpenFOAM` framework to generate a 2D database for machine learning purposes using NACA symmetrical, 4- and 5-digit airfoils. 
For more details, please refer to [nacaFOAM](https://gitlab.lrz.de/2DAeroML/DataGen/nacafoam).

### bluffFOAM

`bluffFOAM` is a CFD simulation pipeline using the `OpenFOAM` framework to generate a 2D database for machine learning purposes using bluff body shapes generated through a self-made 5-digits coding system. 
For more details, please refer to [bluffFOAM](https://gitlab.lrz.de/2DAeroML/DataGen/bluffFOAM).

### Interpolation

`Interpolation` is a postprocessing tool that was used to generate the databases based on the raw simulation output.
For more details, please refer to [Interpolation](https://gitlab.lrz.de/2DAeroML/DataGen/Interpolation).


## ML Flow Field Predictions

This subgroup contains the projects `Example U-Net` and `nacaTransformer` which showcase the application of the `AirfoilMNIST` dataset

### Example U-Net

`Example U-Net` contains code for an architecture initially proposed for another dataset, which was adapted to show the usage of our dataset. 
For more details, please refer to [Example U-Net](https://gitlab.lrz.de/2DAeroML/FieldPredict/U-Net).

### nacaTransformer

`nacaTransformer` includes a novel architecture we are developing. 
For more details, please refer to [nacaTransformer](https://gitlab.lrz.de/2DAeroML/FieldPredict/nacaTransformer).